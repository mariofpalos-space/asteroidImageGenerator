import os
import sys
import cv2  # Recheck if you need this in the future.
import argparse
import subprocess
import numpy as np
import PySimpleGUI as sg    # Not needed if I import the gui_settings?
import matplotlib.pyplot as plt
from matplotlib.image import imread
from PIL import Image, ImageTk, ImageDraw, ImageOps

from modules import gui_functions as gui
from modules import gui_layouts as layouts
from modules.settings_parsing import Settings
from modules import postprocessing_functions as post


# Reading defaults ########################################################

argv = sys.argv
parser = argparse.ArgumentParser()

parser.add_argument("-c", "--customSettingsPath", dest = "customSettingsPath",
        required = False, help = "Path to a custom settings file, relative to project folder.")

args = parser.parse_args()

if args.customSettingsPath:
    settingsFile = args.customSettingsPath
else:
    settingsFile = "settings.txt"


settings = Settings()

try:
    # Tries reading defaults from settings.txt
    # Missing: warning or custom error if settings.txt is read but some
    # setting is missing or wrong.

    settings.readFile(settingsFile) 

    settingsRead = True


except IOError:

    settings.defaults

    settingsFile = ""

    settingsRead = False


# Starting states   #########################################################

# TEST: Making a dictionary out of all these, so it's easier to pass it 
# to functions in modules (to gui_layouts specifically).

toggles = {}

# Normally-off sections.

toggles["albedoVar"]        =   False
toggles["customCamera"]     =   False   # Is this used anywhere?
toggles["postParameters"]   =   False
toggles["imageAnalysis"]    =   False

# Complicated status.

if settings.outDir == "":
    toggles["customOutput"] =   False
else:
    toggles["customOutput"] =   True

if settings.angularCoords:
    toggles["cartesian"]    = False
    defaultCoordType        = "Angular"
else:
    toggles["cartesian"]    = True
    defaultCoordType        = "Cartesian"

toggles["coordsFile"]       =   settings.fromFile
toggles["coordsManual"]     =   not settings.fromFile

if settings.surfaceFunction == "McEwen" or settings.boulderFunction == "McEwen":
    toggles["McEwen"]       = True
else:
    toggles["McEwen"]       = False

if settings.surfaceFunction == "ROLO" or settings.boulderFunction == "ROLO":
    toggles["ROLO"]         = True
else:
    toggles["ROLO"]         = False


# Theme             #########################################################

sg.theme("DarkGray9")


# Layout definition #########################################################

# RENDERING TAB

renderingTab = layouts.renderingTab(settings, toggles, defaultCoordType)

# POST-PROCESSING TAB

recipesList         =   ["Camera SNR analysis", "Camera spectral retrieval analysis",
                            "Image SNR analysis", "Image spectral retrieval analysis",
                            "Image to hyperspectral datacube", "ASPECT's fixed noise"]

postprocessingTab = layouts.postprocessingTab(settings, recipesList, toggles)


# DEFINING WINDOWS #########################################################

def makeMainWindow():

    layout = [[sg.TabGroup(
        [[sg.Tab("Asteroid generation", renderingTab),
        sg.Tab("Post-processing", postprocessingTab)]]
        )], 


        # "Command line"
        [sg.Multiline(size=(150, 10), echo_stdout_stderr=True, reroute_stdout=True, 
            autoscroll=True, background_color='black', text_color='white', key='-MLINE-'),

        # Close button
        sg.Push(), sg.Button("Close"), sg.Push()],

        ]

    return sg.Window("Asteroid Image Simulator", layout, resizable = True, finalize = True)


def makeSpecRetrievalWindow():

    layout = layouts.spectralRetrievalPopup()

    # Can't remember what "finalize" does.
    return sg.Window("Image spectral Retrieval", layout, resizable = True, finalize = True)


# Initializes the main window, but not the pop-up, not for now.
main_window, spec_retrieval_window = makeMainWindow(), None


# Loop ###########################################################################


if settingsRead:

    print("Default settings read from settings.txt.")

else:

    print("Tried to, and failed to, read defaults from settings.txt")

colorRampError  =   False
postImageLoaded =   False

while True:

    window, event, values = sg.read_all_windows()

    if event == sg.WIN_CLOSED or event == "Close": 
        # User closes window or clicks cancel

        window.close()

        if window == spec_retrieval_window:
            spec_retrieval_window = None
        elif window == main_window:
            break

    # Pop-up test
    if event == "Pop-up":

        spec_retrieval_window = makeSpecRetrievalWindow()


    if event == "noiseBlackLevel" or event == "noiseWhiteLevel":
        # User updates one of the colorRamp values.

        if values["noiseBlackLevel"] >= values["noiseWhiteLevel"] and not colorRampError:

            print("Black level should be smaller than white level")
            colorRampError = True

        if values["noiseBlackLevel"] < values["noiseWhiteLevel"] and colorRampError:
            
            print("Black/White levels corrected")
            colorRampError = False

    if event == "wlSampleStart":

        if values["wlSampleStart"] < values["wlStart"]:

            print("Warning: 'Sample wl start' should be higher than 'Initial wavelenght'.")

    if event == "wlSampleEnd":

        if values["wlSampleEnd"] > values["wlEnd"]:
        
            print("Warning: 'Sample wl end' should be lower than 'Final wavelenght'.")



    if event == "output":

        # User checks "Custom output path".

        toggles["customOutput"] = not toggles["customOutput"] 
        window["customOutputSection"].update(visible = toggles["customOutput"])

    if event == "coordinateType":

        toggles["cartesian"] = not toggles["cartesian"]
        window["cartesianSubsection"].update(visible = toggles["cartesian"])
        window["angularSubsection"].update(visible = not toggles["cartesian"])
                

    if event == "file":

        # User checks "Coordinates from file".

        toggles["coordsFile"] = not toggles["coordsFile"]
        window["fileCoordSection"].update(visible = toggles["coordsFile"]) 
        window["rot"].update(disabled = toggles["coordsFile"])

        toggles["coordsManual"] = not toggles["coordsManual"]
        window["coordSection"].update(visible = toggles["coordsManual"])

    if event == "Preview geometry":
        
        if not toggles["coordsFile"]:

            # Single observation plot

            if values["coordinateType"] == "Angular":

                #print("Not ready for angular yet")

                camLat  = float(values["camLat"])
                camLon  = float(values["camLon"])
                camDist = float(values["camDist"])
                
                sunLat  = float(values["sunLat"])
                sunLon  = float(values["sunLon"])

                gui.drawMap([(camLat, camLon, camDist)], [sunLat, sunLon], angular = True)

            else:

                camX = float(values["camX"])
                camY = float(values["camY"])
                camZ = float(values["camZ"])

                sunX = float(values["sunX"])
                sunY = float(values["sunY"])
                sunZ = float(values["sunZ"])

                gui.drawMap([(camX, camY, camZ)], [sunX, sunY, sunZ])

        else:

            # Here I need to read the coordinates from the file.
            # Make sure to make one version for cartesian and another for spherical.

            # Also if illumination changes then you need multiple plots. Ugh.

            with open(values["-COORDSFILE-"]) as f:
                lines = f.readlines()

            coords = []

            if values["coordinateType"] == "Angular":

                if len(lines[0].split()) != 6:
                    print("Datafile seems to have the wrong number of columns (should be 6).")

                else:
                    # Camera coordinates
                    for line in lines[1:]:  # Skipping header.

                        line = line.split()

                        camLat  =   float(line[0])
                        camLon  =   float(line[1])
                        camDist =   float(line[2])

                        coords.append((camLat, camLon, camDist))

                    # Sun coordinates
                    ## Problem: what to do when there are different values?
                    ## Using only the first value for the sun for now.

                    firstLine = lines[1].split()

                    sunLat  =   float(firstLine[3])
                    sunLon  =   float(firstLine[4])

                    gui.drawMap(coords, [sunLat, sunLon], angular = True)


            else:
                # Cartesian coordinates

                if len(lines[0].split()) != 7:
                    print("Datafile seems to have the wrong number of columns (should be 7).")

                else:
                    # Camera coordinates
                    for line in lines[1:]:  # Skipping header.

                        # Split the data string on its whitespaces
                        line = line.split()

                        camX    =   float(line[0])
                        camY    =   float(line[1])
                        camZ    =   float(line[2])

                        coords.append((camX, camY, camZ))

                    # Sun coordinates
                    ## Problem: what to do when there are different values?
                    ## Using only the first value for the sun for now.

                    firstLine = lines[1].split()

                    sunX    =   float(firstLine[3])
                    sunY    =   float(firstLine[4])
                    sunZ    =   float(firstLine[5])

                    gui.drawMap(coords, [sunX, sunY, sunZ])


    """
    if event == "preview":
        # Unfinished

        try:
            filename = values["-COORDSFILE-"]
            obs = sum(1 for line in open(filename))
            print(f"{obs} observations, ")

        except:
            print("File can't be read or found.") 
    """

    if event == "surfaceFunction" or event == "bouldersFunction":

        # User changes surface photometric function.

        # First process the toggles

        if values["surfaceFunction"] == "ROLO" or values["bouldersFunction"] == "ROLO":

                toggles["ROLO"] = True

        else:

                toggles["ROLO"] = False

        if values["surfaceFunction"] == "McEwen" or values["bouldersFunction"] == "McEwen":

                toggles["McEwen"] = True

        else:

                toggles["McEwen"] = False

        # And then show or hide the menus.


        if toggles["ROLO"]:
            window["ROLOSection"].update(visible = True)
        else:
            window["ROLOSection"].update(visible = False)
            
        if toggles["McEwen"]:
            window["McSection"].update(visible = True)
        else:
            window["McSection"].update(visible = False)




    if event == "camera":

        # User changes camera selection

        def updateCamera(XRes, YRes, XFoV, YFoV):
            # Updates the four fields of the camera in one go.

            window["XRes"].update(value = XRes)
            window["YRes"].update(value = YRes)

            window["XFoV"].update(value = XFoV)
            window["YFoV"].update(value = YFoV)

        chosenCamera = values["camera"]

        if chosenCamera == "None":

            updateCamera("", "", "", "")

        elif chosenCamera == "ASPECT-VIS":

            updateCamera("1024", "1024", "0.174533", "0.174533")

        elif chosenCamera == "ASPECT-VIS-GREEN":

            updateCamera("1024", "1024", "0.174533", "0.174533")

        elif chosenCamera == "ASPECT-NIR":

            updateCamera("640", "512", "0.116937", "0.116937")


    if event == "albedoVar":

        # User checks "Albedo variations".

        toggles["albedoVar"] = not toggles["albedoVar"]
        window["albedoVarSubsection"].update(visible = toggles["albedoVar"])

    if event == "Update preview":

        gui.renderAlbedoPreview(
            blenderPath     =   values["-BLENDERPATH-"],
            noiseSeed       =   values["noiseSeed"],
            noiseScale      =   values["noiseScale"],
            noiseDetail     =   values["noiseDetail"],
            noiseRoughness  =   values["noiseRoughness"],   
            noiseBlackLevel =   values["noiseBlackLevel"],     
            noiseWhiteLevel =   values["noiseWhiteLevel"],      
            )


        # Loads/reloads albedo.png
        window["-ALBEDOPREVIEW-"].update(source = "modules/albedo.png")




    # Load image / Box moved
    # Why is this together? Does the image need to be reloaded every time?

    if (event == "Load image" 
        or event == "xBox" or event == "yBox"
        or event == "preview_scale"
        or event == "boxXPosition" or event == "boxYPosition"):

        # The user loads an image or updates the size or position of the box.

        # In use?
        maxHeight = 512 # Limits vertical size of preview, so that it doesn't mess up the gui.

        selectedImage = values["-IMAGESELECTION-"]
        previewScale = values["preview_scale"]
        print(previewScale)

        try:

            img = Image.open(selectedImage)

            imgBW = ImageOps.grayscale(img)

            postImageLoaded = True  # Is this needed?

            if event == "Load image":
                print(f"{selectedImage} loaded.")

            # Test
            window["popup_parameters"].update(visible = True)

            #window["selectionBoxSubsection"].update(visible = True)
            #open_window()

            # TBD: check if resolutions match

            imgWidth, imgHeight = img.size
            print("One")

            """
            print(values[post_XRes])    # This fails

            # Should I create a value in the popup and .update here?

            print("One point five")

            if (imgWidth != int(values["post_XRes"]) or
                    imgHeight != int(values["post_YRes"])):

                print("Warning: resolution of camera and image mismatch.")
            """


            aspectRatio     =   int(imgWidth / imgHeight)
            #scaling         =   float(maxHeight / imgHeight)
            # Not sure about the following:
            scaling = previewScale

            boxXPosition    =   int(values["boxXPosition"]) * 0.01
            boxYPosition    =   int(values["boxYPosition"]) * 0.01


            originX         =   imgWidth * boxXPosition
            originY         =   imgHeight * boxYPosition 

            boxWidth        =   int(values["xBox"])
            boxHeight       =   int(values["yBox"])

            leftX           =   int(originX - int(boxWidth / 2))
            topY            =   int(originY - int(boxHeight / 2))
            rightX          =   int(originX + int(boxWidth / 2))
            bottomY         =   int(originY + int(boxHeight / 2))

            print("Two")

            # Scaled sizes, for the gui preview

            #resizedWidth    =   maxHeight * aspectRatio
            #resizedHeight   =   maxHeight
            resizedWidth    =   int(imgWidth * scaling)
            resizedHeight   =   int(imgHeight * scaling)


            resized_img     =   img.resize((resizedWidth, resizedHeight), Image.ANTIALIAS)

            scaled_originX  =   originX * scaling
            scaled_originY  =   originY * scaling 

            scaled_boxWidth     =   boxWidth * scaling
            scaled_boxHeight    =   boxHeight * scaling

            scaled_leftX    =   leftX * scaling
            scaled_topY     =   topY * scaling
            scaled_rightX   =   rightX * scaling
            scaled_bottomY  =   bottomY * scaling


            draw = ImageDraw.Draw(resized_img)

            draw.rectangle(
                (scaled_leftX, scaled_topY, scaled_rightX, scaled_bottomY),
                width = 1,
                )

            draw = ImageDraw.Draw(img) # What does this do? # Adds the box, I guess!
            # But why is it img and not resized_img?

            print("Three")  # I can't access all these values from the popup, why?

            #window["-POPUP_SPECTRALIMAGE-"].update(data = ImageTk.PhotoImage(image = img))
            window["-POPUP_SPECTRALIMAGE-"].update(data = ImageTk.PhotoImage(image = resized_img))

            print("Four")  # I can't access all these values from the popup, why?


        #except:

        #    print("Can't load image")

        except Exception as e:

            print(e)

    if event == "recipe":

        # User selects a post-processing recipe.
        # I really need a function here.
        # (And move it to the module later).

        if not toggles["postParameters"]:

            toggles["postParameters"] = True
            window["postParameters"].update(visible = toggles["postParameters"])
            window["runButton"].update(visible = toggles["postParameters"])


            """
            # Maybe it won't work for all workflows. Leave it for now.
            if values["-POSTINPUTFOLDER-"] != "":

                upper = "/".join(values["-POSTINPUTFOLDER-"].split("/")[:-1]) + "/"

                window["prePath"].update(upper)
            """


        chosenRecipe = values["recipe"]

        # Hiding/unhiding analyisis menu.

        if chosenRecipe == "Image spectral retrieval analysis":

            toggles["imageAnalysis"] = True

        else:

            toggles["imageAnalysis"] = False

        window["imageAnalysisSection"].update(visible = toggles["imageAnalysis"])



        postInputs = ["post_XRes", "post_YRes", "post_XFoV", "post_YFoV",
                        "apertureDiameter", "opticsTransmission", 
                        "distanceToSun", "surfaceAlbedo", "imageAlbedo",
                        "spectralTransmissionStrength", "spectralTransmissionWindowWidth",
                        "wlStart", "wlEnd",
                        "readNoise", "darkCurrent", "integrationTime", "fullWellCapacity",
                        "snrWl", "wlSampleStart", "wlSampleEnd", "wlSampleStep"]


        if chosenRecipe == "Camera SNR analysis":

            description = "Analyses the signal-to-noise ratio (SNR) of the camera unit."
            window["-RECIPEDESCRIPTION-"].update(description)

            disabledInputs = ["imageAlbedo", "wlSampleStart", "wlSampleEnd", "wlSampleStep"]

            gui.disablePostInputs(window, postInputs, disabledInputs)

            window["-POSTOUTPUTFOLDER-"].update("Camera SNR analysis")



        if chosenRecipe == "Camera spectral retrieval analysis":

            description = """Shows how the spectral retrieval of the target would look like
with the specified camera and noise parameters."""
            window["-RECIPEDESCRIPTION-"].update(description)

            disabledInputs = ["snrWl", "imageAlbedo"]

            gui.disablePostInputs(window, postInputs, disabledInputs)

            window["-POSTOUTPUTFOLDER-"].update("Camera spectral retrieval analysis")



        if chosenRecipe == "Image SNR analysis":

            description = "Shows how a perfect image would change with realistic noise from the detector."
            window["-RECIPEDESCRIPTION-"].update(description)

            disabledInputs = ["wlSampleStart", "wlSampleEnd", "wlSampleStep"]

            gui.disablePostInputs(window, postInputs, disabledInputs)

            window["-POSTOUTPUTFOLDER-"].update("Image SNR analysis")



        if chosenRecipe == "Image spectral retrieval analysis":

            description = "Spectral retrieval of a region of a specific image."
            window["-RECIPEDESCRIPTION-"].update(description)

            disabledInputs = ["snrWl"]


            gui.disablePostInputs(window, postInputs, disabledInputs)

            window["-POSTOUTPUTFOLDER-"].update("Image spectral retrieval analysis")

            toggles["imageAnalysis"] = True
            window["imageAnalysisSection"].update(visible = toggles["imageAnalysis"])



        if chosenRecipe == "Image to hyperspectral datacube":

            description = """Builds hyperspectral datacubes out of  perfect images 
and target spectra, and exports as PNG image stack."""

            window["-RECIPEDESCRIPTION-"].update(description)

            disabledInputs = ["snrWl"]
    
            gui.disablePostInputs(window, postInputs, disabledInputs)

            window["-POSTOUTPUTFOLDER-"].update("Hyperspectral datacube")



        if chosenRecipe == "ASPECT's fixed noise":

            description = "TBD."
            window["-RECIPEDESCRIPTION-"].update(description)

            disabledInputs = postInputs # Everything off.

            gui.disablePostInputs(window, postInputs, disabledInputs)

            window["-POSTOUTPUTFOLDER-"].update("Fixed noise")


    if event == "Load render metadata":

        # Need to load from settings: FoV, detectorResolution... What else?
        # surfaceAlbedo according to the notebook. imageAlbedo too.

        metadata = post.importMetaData(values["-POSTINPUTFOLDER-"]) 
        
        print("X Res and Y Res updated with render metadata.")
        res = metadata["detectorResolution"]
        window["post_XRes"].update(value = int(res[0]))
        window["post_YRes"].update(value = int(res[1]))

        print("X FoV and Y FoV updated with render metadata.")
        fov = metadata["FoV"]
        window["post_XFoV"].update(value = fov[0])
        window["post_YFoV"].update(value = fov[1])

        print("imageAlbedo updated with render metadata.")
        window["imageAlbedo"].update(metadata["imageAlbedo"])
        
            
    if event == "Run":
        # User has selected and ran a post-processing recipe.

        inputPath =  values["-POSTINPUTFOLDER-"]
        outputPath = inputPath.replace("Original", values["-POSTOUTPUTFOLDER-"])

        # Create the folder if it doesn't exist already.
        if not os.path.isdir(outputPath):
            os.makedirs(outputPath)

        # First: e⁻ count.
        # This conversion of string to int to np to int again has to go.

        extraWL =   int(np.ceil(int(values["spectralTransmissionWindowWidth"])/2))
        wlStart =   int(values["wlStart"]) - extraWL
        wlEnd   =   int(values["wlEnd"]) + extraWL
        wlArray =   np.arange(wlStart+extraWL, wlEnd-extraWL, 1)
        albArr  =   np.arange(0,0.21,0.01)
        itArr   =   np.arange(0,101,1)
        
        flatConvFunc, finalConvFunc = post.eConversion(au = float(values["distanceToSun"]), 
                FoV = [float(values["post_XFoV"]), float(values["post_YFoV"])],
                apertureD = float(values["apertureDiameter"]),
                detectorRes = [float(values["post_XRes"]), float(values["post_YRes"])], 
                opticsT = float(values["opticsTransmission"]),
                wlStart = wlStart, wlEnd = wlEnd,
                spectralTransmissionStrength = float(values["spectralTransmissionStrength"]),
                # Should this be an int? I just did it because it wouldn't stop protesting.
                # If it is, an error or warning should pop up if a float is written there.
                spectralTransmissionWindowWidth = int(values["spectralTransmissionWindowWidth"]),
                #targetSpectra = targetSpectra
                targetSpectra = values["-TARGETSPECTRA-"],
                quantumEffData = values["-QUANTUMEFFICIENCIES-"],
            )


        chosenRecipe = values["recipe"]

        if chosenRecipe == "Camera SNR analysis":

            # The selected recipe is Camera SNR analysis.

            snrSpec, snrAlb, snrIt = post.cameraSNRAnalysis(
                
                # surfaceAlbedo could be re-re-named to surfaceAlbedo.
                surfaceAlbedo = float(values["surfaceAlbedo"]),
                finalConvFunc = finalConvFunc, 
                wlArray = wlArray,
                albArr = albArr,
                itArr = itArr,
                readNoise = float(values["readNoise"]),
                darkCurrent = float(values["darkCurrent"]),
                integrationTime = float(values["integrationTime"]),
                fullWellCapacity = float(values["fullWellCapacity"]),
                snrWl = float(values["snrWl"]),
                )


            # Why the hell is the text being printed only after the plots are closed?
            # flush doesn't work, time.sleep() doesn't either.


            # SNR as a function of wavelength

            gui.plotAndSave(
                (wlArray, snrSpec),
                title = "SNR vs. Wavelength", 
                xLabel = "Wavelength (nm)", yLabel = "SNR", 
                savePath = outputPath, saveTitle = "SNR-Wl"
                )

            gui.saveRawData(wlArray, snrSpec,
                savePath = outputPath, saveTitle = "SNR-Wl")
                

            plt.figure()


            # SNR as a function of surface albedo.


            gui.plotAndSave(
                (albArr, snrAlb),
                title = "SNR vs. Surface albedo",
                xLabel = "Surface albedo", yLabel = "SNR",
                savePath = outputPath, saveTitle = "SNR-Albedo"
                )

            gui.saveRawData(albArr, snrAlb,
                savePath = outputPath, saveTitle = "SNR-Albedo")


            plt.figure()


            # SNR as a function of integration time.

            gui.plotAndSave(
                (itArr, snrIt),
                title = "SNR vs. Integration time",
                xLabel = "Integration time (ms)", yLabel = "SNR",
                savePath = outputPath, saveTitle = "SNR-IntTime"
                )

            gui.saveRawData(albArr, snrAlb,
                savePath = outputPath, saveTitle = "SNR-IntTime")

            plt.figure()


        if chosenRecipe == "Camera spectral retrieval analysis":

            wlSampleStart = int(values["wlSampleStart"])
            wlSampleEnd = int(values["wlSampleEnd"])
            wlSampleStep = int(values["wlSampleStep"])
            wlSample = np.arange(wlSampleStart, wlSampleEnd, wlSampleStep)

            origSpectra, noisySpectra = post.cameraSpectralRetrievalAnalysis(
                targetSpectra = values["-TARGETSPECTRA-"],
                wlArray = wlArray,
                flatConvFunc = flatConvFunc,
                finalConvFunc = finalConvFunc,
                surfaceAlbedo = float(values["surfaceAlbedo"]),
                wlSample = wlSample,
                darkCurrent = float(values["darkCurrent"]),
                integrationTime = float(values["integrationTime"]),
                readNoise = float(values["readNoise"]),
                fullWellCapacity = float(values["fullWellCapacity"]),
                )
            
            gui.plotAndSave(
                (wlArray, origSpectra),
                (wlSample, noisySpectra, 'ro'),
                title = "Spectral retrieval analysis for the camera", 
                xLabel = "Wavelength (nm)", 
                yLabel = "Reflectance, normalized at 550 nm",  
                savePath = outputPath, 
                saveTitle = "CameraSpectralRetrieval",
                )


            gui.saveRawData(wlArray, origSpectra, outputPath, 
                "camSpectralRetrieval-originalSpectra")

            gui.saveRawData(wlSample, noisySpectra, outputPath, 
                "camSpectralRetrieval-noisySpectra")

            plt.figure()


        if chosenRecipe == "Image SNR analysis":

            # I think that, by default, it should operate on every image on the folder.
            # If you only want one, create a folder with one image.

            inputFolder = values["-POSTINPUTFOLDER-"]

            for filename in os.listdir(inputFolder):
                if filename.endswith(".png"):
                    #print(f"First image: {filename}")
                    #break   # Keeping the first only, for now.

                    noisyIm, imH, imNormVal = post.imageSNRAnalysis(
                        imageFilename = filename,
                        foldername = inputFolder,
                        imageAlbedo = float(values["imageAlbedo"]),
                        surfaceAlbedo = float(values["surfaceAlbedo"]),
                        finalConvFunc = finalConvFunc,
                        snrWl = float(values["snrWl"]),
                        readNoise = float(values["readNoise"]),
                        darkCurrent = float(values["darkCurrent"]),
                        integrationTime = float(values["integrationTime"]),
                        fullWellCapacity = float(values["fullWellCapacity"]),
                        )


                    name = f"{outputPath}/{filename.strip('.png')}-noisy.png"
                    print(f"{name} saved.")
                    plt.imsave(name, noisyIm.reshape(imH,-1), cmap="gray", vmin=0, vmax=imNormVal)


        if chosenRecipe == "Image spectral retrieval analysis":

            # wlSample

            wlSampleStart = int(values["wlSampleStart"])
            wlSampleEnd = int(values["wlSampleEnd"])
            wlSampleStep = int(values["wlSampleStep"])
            wlSample = np.arange(wlSampleStart, wlSampleEnd, wlSampleStep)

            # rcData
            
            BWValues = []

            for i in range(leftX, rightX):
                for j in range(topY, bottomY):

                    BWValues.append(imgBW.getpixel((i, j)))

            rcData = np.average(BWValues)
            rcData = rcData / 255   # Normalization
            rcData = np.array([rcData])

            origSpectra, aveSpectra = post.imageSpectralRetrievalAnalysis(
                targetSpectra = values["-TARGETSPECTRA-"],
                wlArray = wlArray,
                flatConvFunc = flatConvFunc,
                finalConvFunc = finalConvFunc,
                rcData = rcData,
                wlSample = wlSample,
                darkCurrent = float(values["darkCurrent"]),
                integrationTime = float(values["integrationTime"]),
                readNoise = float(values["readNoise"]),
                fullWellCapacity = float(values["fullWellCapacity"]),
                )

            gui.plotAndSave(
                (wlArray, origSpectra),
                (wlSample, aveSpectra, 'ro'),
                title = "Spectral retrieval analysis for the image", 
                xLabel = "Wavelength (nm)", 
                yLabel = "Reflectance, normalized at 550 nm",
                savePath = outputPath, 
                saveTitle = "imageSpectralRetrieval",
                )

            resized_img.save(f"{outputPath}/imageAndSelection.png")

            gui.saveRawData(wlArray, origSpectra, outputPath, 
                "imageSpectralRetrieval-originalSpectra")

            gui.saveRawData(wlSample, aveSpectra, outputPath, 
                "imageSpectralRetrieval-aveSpectra")

            plt.figure()



        if chosenRecipe == "Image to hyperspectral datacube":
            
            wlSampleStart = int(values["wlSampleStart"])
            wlSampleEnd = int(values["wlSampleEnd"])
            wlSampleStep = int(values["wlSampleStep"])
            wlSample = np.arange(wlSampleStart, wlSampleEnd, wlSampleStep)

            inputFolder = values["-POSTINPUTFOLDER-"]

            for filename in os.listdir(inputFolder):
                if filename.endswith(".png"):
            
                    print(f"Working on {filename}.")

                    post.imageToHypercube(
                        imageFilename = filename,
                        foldername = inputFolder,
                        imageAlbedo = float(values["imageAlbedo"]),
                        surfaceAlbedo = float(values["surfaceAlbedo"]),
                        finalConvFunc = finalConvFunc,
                        wlSample = wlSample,
                        readNoise = float(values["readNoise"]),
                        darkCurrent = float(values["darkCurrent"]),
                        integrationTime = float(values["integrationTime"]),
                        fullWellCapacity = float(values["fullWellCapacity"]),
                        outputFolder = outputPath,
                        outputFilename = f"{filename.strip('.png')}-hyper"
                        )

        if chosenRecipe == "ASPECT's fixed noise":
            
            print(f"Not finished yet.")

            inputFolder = values["-POSTINPUTFOLDER-"]

            for filename in os.listdir(inputFolder):
                if filename.endswith(".png"): # Skipping non-image files

                    post.fixedNoise(filename, inputFolder)


    if event == "Render" or event == "Preview":

        # User clicks the "Render" button.
       
        #print(values)

        # Errors:

        if values["-BLENDERPATH-"] == "":
            # Blender executable missing.

            print("ERROR: Blender executable path required.")

        elif toggles["customOutput"] and values["-OUTDIR-"] == "":
            # Custom output selected but no path provided.

            print("ERROR: Custom output selected but none provided.")

        elif toggles["coordsFile"] and values["-COORDSFILE-"] == "":

            print("ERROR: Coordinates file selected but none provided.")

        elif values["XRes"] == "" or values["YRes"] == "":

            print("ERROR: Camera resolution missing")

        elif values["XFoV"] == "" or values["YFoV"] == "":

            print("ERROR: Camera FoV missing")

        elif values["scale"] == "":

            print("ERROR: Scale not selected (set 1 for default)")

        elif values["rot"] == "":

            print("ERROR: Rotation not selected (set to 0 for default)")

        elif values["albedo"] == "":

            print("ERROR: Surface albedo not selected.")

        # Commands to be passed to blenderControl.py:

        else:

            # Commands are passed as a list of **strings**.
            # Maybe floats are ok? But booleans are definitely not.

            command = [
                    values['-BLENDERPATH-'],
                    "--background", "./asteroidImageGenerator.blend",
                    "--python", "./blenderControl.py",
                    "--"
                    ]


            if toggles["customOutput"]:
                
                command += ["--outDir", values['-OUTDIR-']]

            # --angularCoordinates will be here.

            if values["coordinateType"] == "Angular":

                command += ["--angularCoordinates"]

            if toggles["coordsFile"]:

                command += ["--fromFile", "--positionalData", values['-COORDSFILE-']]

            else:

                command += ["--coordinates"]

                if values["coordinateType"] == "Cartesian":

                    command += [
                            f"[{values['sunX']}, {values['sunY']}, {values['sunZ']}]",
                            f"[{values['camX']}, {values['camY']}, {values['camZ']}]"
                            ]

                elif values["coordinateType"] == "Angular":

                    command += [
                            f"[{values['sunLat']}, {values['sunLon']}]",
                            f"[{values['camLat']}, {values['camLon']}, {values['camDist']}]"
                            ]

            command += ["--imageAlbedo", values['albedo']]  # Not a string, no problems?
                                                            # Apparently not.

            command += ["--surfaceFunctionName", values['surfaceFunction'],
                        "--boulderFunctionName", values['bouldersFunction']]

            if toggles["ROLO"]:

                command += [
                        "--ROLOG0", values['ROLOG0'],
                        "--ROLOG1", values['ROLOG1'],
                        "--ROLOA0", values['ROLOA0'],
                        "--ROLOA1", values['ROLOA1'],
                        "--ROLOA2", values['ROLOA2'],
                        "--ROLOA3", values['ROLOA3'],
                        "--ROLOA4", values['ROLOA4'],
                        ]
            
            if toggles["McEwen"]:

                command += [
                        "--McEEpsilon", values['McEEpsilon'],
                        "--McEKsi", values['McEKsi'],
                        "--McEEta", values['McEEta'],
                        "--McEBeta", values['McEBeta'],
                        "--McEGamma", values['McEGamma'],
                        "--McEDelta", values['McEDelta']
                        ]

            command += ["--target", values['target']]

            command += ["--proceduralDetail", str(values['detail'])]

            # NEW!
            command += ["--satellite", str(values["satellite"])]


            command += ["--systemRotation", values['rot']]
            command += ["--systemScale", values['scale']]

            command += ["--resolution", values['XRes'], values['YRes']]
            command += ["--fov", values['XFoV'], values['YFoV']]

            if values["albedoVar"]:

                command += ["--albedoVariations"]

                command += [
                    # Sliders return floats, but boxes return strings.
                    "--albedoRange", str(values["albedoRange"]),
                    "--noiseScale", values["noiseScale"],
                    "--noiseSeed", str(values["noiseSeed"]),
                    "--noiseDetail", str(values["noiseDetail"]),
                    "--noiseRoughness", str(values["noiseRoughness"]),
                    "--noiseWhiteLevel", str(values["noiseWhiteLevel"]),
                    "--noiseBlackLevel", str(values["noiseBlackLevel"]),
                        ]


            # Could be good to transform all items inside command to strings here.

            if event == "Preview":
                print(command)
                #print(type(command))
                #for item in command:
                #    print(item)
                #    print(type(item))

            elif event == "Render":
                print("Rendering!")
                subprocess.run(command)

window.close()
