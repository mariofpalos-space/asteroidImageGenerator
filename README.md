IMPORTANT: in order to clone the repository, [Git LFS](https://git-lfs.com/) need to be installed. If not, blend files will get corrupted but throw no warnings.

Usage: 

From the command line, run main.py in Python 3, and if you agree with the settings, accept and run. The results will be saved in /Renders. In order to change the settings, edit the file called settings.txt.

Alternatively, run gui.py in order to access a graphical user interface. The file settings.txt will be used, if available, as a source for default values.

Check the files inside the documentation folder more detailed information.

Copyright 2021 Mario F. Palos, Antti Penttil�, Tomas Kohout (Institute of Geology of the Czech Academy of Sciences and University of Helsinki)
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
